// ----------------------------
// projects/collatz/Collatz.c++
// Copyright (C) 2014
// Glenn P. Downing
// ----------------------------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <utility>  // make_pair, pair

// -------
// defines
// -------

#ifdef ONLINE_JUDGE
    #define NDEBUG
#endif

int* lazy_cache = new int[999999];

// ------------
// collatz_read
// ------------

std::pair<int, int> collatz_read (std::istream& r) {
    int i;
    r >> i;
    if (!r)
        return std::make_pair(0, 0);
    int j;
    r >> j;
    return std::make_pair(i, j);}

// ------------
// collatz_eval
// ------------

int collatz_eval (int i, int j) {
    // <your code>

    assert(i>0); //pre condition checker; It should pass these
    assert(j>0); //and argument checker

    int result = 0;
    if(i>j){
        /*int temp = i;
        i = j;
        j = temp;*/
        i^=j;
        j^=i;
        i^=j;
    }
    while(i <= j){
        int cycle_length = 1;
        /*Public repo test dosen't take in account that we may
        be testing an 32 bit int, but when we calculate the cycle 
        length we very well may be exceeding the range of a 32 bit
        integer therefore I have to use a long to guarantee 
        accuracy within the public repo test. Sphere will not give 
        us an int that will exceed the bounds of an Integer when 
        calculating*/
        if((2*i) <= j){
            ++i;
        }
        else{
            long n = i; 
            if(lazy_cache[i-1]){
                cycle_length = lazy_cache[i-1];
            }
            else{
                while(n>1){
                    if((n%2) == 0){
                        n = (n/2);
                        ++cycle_length;
                    }
                    else{
                        n = n+(n >> 1)+1;
                        ++++cycle_length;
                    }
                }
                assert(cycle_length>0);
                lazy_cache[i-1] = cycle_length;
            }

            if(cycle_length>result)
                result = cycle_length;
            ++i;
        }
    }
    assert(result>0); //post condition checker
    return result;}

// -------------
// collatz_print
// -------------

void collatz_print (std::ostream& w, int i, int j, int v) {
    w << i << " " << j << " " << v << std::endl;}

// -------------
// collatz_solve
// -------------

void collatz_solve (std::istream& r, std::ostream& w) {
    for(int i = 1; i<=999999; ++i){
        lazy_cache[i-1] = 0;
    }

    while (true) {
        const std::pair<int, int> p = collatz_read(r);
        if (p == std::make_pair(0, 0))
            return;
        const int v = collatz_eval(p.first, p.second);
        collatz_print(w, p.first, p.second, v);}
    
    }

// ----
// main
// ----

int main () {
    using namespace std;
    collatz_solve(cin, cout);
    delete [] lazy_cache;
    lazy_cache = NULL;
    return 0;}
